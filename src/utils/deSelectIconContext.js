import { createContext } from 'react'

const deSelectIconContext = createContext(['false', () => {}])

export default deSelectIconContext
