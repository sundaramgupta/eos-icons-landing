import { createContext } from 'react'

const selectIconContext = createContext()

export default selectIconContext
